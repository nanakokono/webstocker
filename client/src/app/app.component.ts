import { Component } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
// import { GetWebInformationService } from 'get-web-information.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // providers: [GetWebInformationService]
})

export class AppComponent {

  public item: Observable<{}>; // 追加


  constructor(db: AngularFireDatabase) {
    this.item = db.object('item').valueChanges();
  }

  public onSubmit(form: any): void {
    if (form) {
      const url = form.value;

      // this.item.push(url);
      form.value = '';
      console.log(form);
      console.log(this);
    }
  }

  private getInformation(url): void {
  }
}

